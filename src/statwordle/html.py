"""HTML elements for statwordle outputs.
"""

STYLE = """
<style>
.game-tile {
    width: 2em;
    height: 2em;
    line-height: 2rem;
    box-sizing: border-box;
    text-transform: uppercase;
    justify-content: center;
    align-items: center;    
    display: inline-flex;
    color: white;
}
.game-row {
    # display: block;
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    grid-gap: 5px;   
    width: 10em;
    margin: 0.5em 0.5em;
}
.gray {
    background-color: #939598;
}
.orange {
    background-color: #b59f3b;
}
.green {
    background-color: #538d4e;
}
</style>
"""


LIGHT_MODE = {
  "-": "⬜",
  "x": "⬜",
  "o": "🟨",
  "g": "🟩"
}

DARK_MODE = {
  "-": "⬛",
  "x": "⬛",
  "o": "🟨",
  "g": "🟩"
}

BOOTSTRAP_TEMPLATE="""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>{title}</title>
</head>
<body>
{content}
</body>
</html>
"""

def write_html(fp, content="", title=None, template=None, **kwargs):
    """Write some HTML content
    """

    if template is None:
        template = BOOTSTRAP_TEMPLATE

    title="StatWordle HTML Output"

    html_code = template.format(content=content, title=title, **kwargs)

    # Check if fp is writable:
    if hasattr(fp, 'write'):
        fp.write(html_code)
    else:
        # Assume fp is a filename:
        with open(fp, "wt", encoding='utf-8') as f:
            f.write(html_code)
