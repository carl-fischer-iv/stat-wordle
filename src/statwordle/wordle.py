from random import choice
from statwordle.util import entropy, letter_histogram, word_filter
from statwordle.knowledge import Knowledge, KnowledgeSet
from string import ascii_lowercase
from collections import defaultdict
from collections.abc import Iterable
from typing import Optional
from .words import OG_ANSWERS, DICTIONARY, DICTIONARY_ENTROPY, ROOT_ENTROPY
from typing import List, Tuple 
from .html import DARK_MODE


class WordleSolver:
  """A Class to help solve Wordle optimally.
  """
  # Knowledge is stored as a set of Knowledge objects
  _knowledge = None
  words = set()
  alphabet = set(ascii_lowercase)
  hard_mode = False
  possible_words = set()

  def __init__(self, words: List[str], hard_mode: bool = False):
    self.words = set(words)
    self.possible_words = set(words)
    self._knowledge = KnowledgeSet()
    self.hard_mode = hard_mode

    assert all([word_filter(w) for w in words]), "All words must be 5 characters"
  
  @property 
  def possible_letters(self):
    """All the letters in the possible words.
    """
    return set("".join(self.possible_words))

  def word_scores(self):
    word_scores = defaultdict(lambda: 0)
    for letter in self.unknown_letters & self.possible_letters:
        for word in self.words:
            if letter in word: 
                word_scores[word] += 1
    score_sets = defaultdict(lambda: set())
    for w,v in word_scores.items():
      score_sets[v].add(w)
    return score_sets

  def summary(self):
    result = []
    result.append(str(self._knowledge))
    if len(self.possible_words) == 1: 
      result.append("***** SOLUTION: " + list(self.possible_words)[0].upper() + "\n")
    elif len(self.possible_words) < 5:
      result.append("***** POSSIBLE WORDS: " + " ".join(self.possible_words))
    return "\n".join(result)
    
  def suggest_guesses(self, num=5, include_scores=False, common_factor=1.0):
    """Suggests some good guesses.

    @param num [int]: Number of suggestions to make
    """

    if len(self.possible_words) == 1:
      return list(self.possible_words)
    if self.hard_mode:
      source_dictionary=self.possible_words
    else:
      source_dictionary=DICTIONARY
    
    # Use pre-calculated entropy if knowledge is empty.
    if len(self._knowledge)==0:
      e = lambda w: ROOT_ENTROPY[w]
    else: 
      hists = self.get_possible_histograms()
      e = lambda w: entropy(w, hists)

    best =  sorted([(e(word) + (common_factor * DICTIONARY_ENTROPY.get(word, 0.0)),
                     word) 
                    for word in source_dictionary])[:-(num+1):-1]

    if include_scores:
      return best
    
    return [x[1] for x in best]

  def update_possible_words(self, kset: KnowledgeSet = None):
    kset = kset or self._knowledge
    self.possible_words = set([w for w in self.possible_words if kset.is_possible(w)])

  def get_possible_sets(self):
    return list(map(set, zip(*self.possible_words)))

  def get_possible_histograms(self):
    """Returns a set of histograms of letters for each position based on the 
    list of all possible words
    """
    return [letter_histogram(position) for position in zip(*self.possible_words)]
      

  @property
  def unknown_letters(self):
    """Returns the letters we don't know about yet.
    """
    return self._knowledge.unknown_letters

  @property
  def unknown_positions(self):
    """Returns the letters we don't know about yet.
    """
    return {s for s,k in self._knowledge.items() if k.spot == 0}


  def add_knowledge(self, letter: str, spot: int = 0):
    """Adds something we know to the knowledge store

    letter: The letter we know about
    spot: 1-5  -> Known position 
          0    -> Unknown
          none -> Not present
    """
    self._knowledge.add_knowledge(letter, spot)
    self.update_possible_words(KnowledgeSet([Knowledge(letter, spot)]))
  
  def add_guess(self, guess:str, response:str):
    kset = KnowledgeSet()
    for i, (g,r) in enumerate(zip(guess, response)):
      if r == 'g':
        kset.add(Knowledge(g, i+1))
      if r == 'o':
        kset.add(Knowledge(g, -(i+1)))
      if r in 'x-': 
        kset.add(Knowledge(g, None))
    kset.clean()
    self.update_possible_words(kset)
    for k in kset:
      self._knowledge.add(k)
    # self._knowledge.clean()

  def score(self, word):
    """Scores a word based on all the knowledge we have
    """
    return sum([k.score(word) for k in self._knowledge])
  
  def is_possible(self, word: str) -> bool:
    """Checks if a word is possible given what we know
    """
    return all([k.is_possible(word) for k in self._knowledge])


class WordleGame:
  answer: str = ''
  guesses: int = 0
  guess_history: List[Tuple] = None
  MAX_GUESSES: int = 6
  LENGTH: int = 5

  def __init__(self, answer: Optional[str] = None):
    self.answer = answer or choice(DICTIONARY)
    self.guesses = 0
    self.guess_history = []
  
  def handle_guess(self, guess:str) -> str:
    if self.guesses >= self.MAX_GUESSES:
      raise ValueError(f"You have exceeded {self.MAX_GUESSES}")
    if len(guess) != self.LENGTH:
      raise ValueError(f"Guesses must be {self.LENGTH} characters, not {len(guess)}")

    result = [None] * self.LENGTH
    ans = list(self.answer)
    for pos, letter in enumerate(guess):
      if self.answer[pos] == letter:
        ans[pos] = None
        result[pos] = 'g'
    for pos, letter in enumerate(guess):
      if result[pos] is None and letter in ans:
        result[pos] = 'o'
        ans.remove(letter)
    for pos, letter in enumerate(guess):
      if result[pos] is None and letter not in ans:
        result[pos] = '-'

    result = "".join(result)
    self.guesses += 1
    self.guess_history += [(guess, result)]

    return result

  def show_results(self, pad = True, censor=False, mode=DARK_MODE):
    color_map = {
      '-': "gray",
      'o': "orange",
      "g": "green"
    }
    items  =[]
    pre=""
    if pad:
      blanks = 6-len(self.guess_history)
      guess_history = self.guess_history + blanks * [('     ', '-----')]
    for guess, result in guess_history:
      if censor: 
        guess='-----'
      items.append("<div class='game-row'>")
      items.extend([f"<div class=\"{color_map[r]} game-tile\">{g}</div>"
        for g,r in zip(guess, result)])
      items.append("</div>")
    return "".join(items)

  def shared_results(self, ndx=None, hard_mode=True, mode=DARK_MODE): 
    """Generates the unicode sharable results for this word."""
    if hard_mode:
      star="*"
    else:
      star=""
    if ndx is not None:
      pre = f"Wordle {ndx} {len(self.guess_history)}/6{star}\n\n"
    else:
      pre=f"Wordle guessing word {self.answer}\n\n"
    pre += "\n".join(["".join([mode[r] for r in result]) 
                     for guess, result in self.guess_history])
    return f"<pre>{pre}</pre>"

class PlayGame:
  """This class plays an entire game, taking a randomly chosen word from the top N
  """
  def __init__(self, word, n_choice=10, common_factor=1.0, **kwargs):
    self.ws = WordleSolver(DICTIONARY, **kwargs)
    self.wg = WordleGame(word)
    self.n_choices = n_choice
    self.common_factor = common_factor
    assert word in DICTIONARY, f"{word} is not in the dictionary"

  def play(self):
    """Play the game and return the results.

    retrns N: The number of tries required.
    """
    for i in range(6):
      guess = choice(self.ws.suggest_guesses(self.n_choices, 
                     common_factor=self.common_factor))
      score = self.wg.handle_guess(guess)
      self.ws.add_guess(guess, score)
      # print(f"{i+1}: {guess} / {score}")
      if score=='ggggg': 
        return i+1

    return False


if __name__ == "__main__":
  ws = WordleSolver(DICTIONARY)
  ws.add_knowledge('a', 1)
  ws.add_knowledge('p', 2)
  ws.add_knowledge('p', 3)
  ws.add_knowledge('y', None)
  print(ws.possible_words)


