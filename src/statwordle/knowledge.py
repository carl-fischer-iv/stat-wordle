from typing import List
from string import ascii_lowercase
from statwordle.util import letter_histogram
from logging import info
# Knowledge represents the knowledge about letters in wordle.

class Knowledge():
  # Things we want to store:
  # Letter, position (green tile)
  # Letter is in the word (orange), but not in the guessed position
  # Letter is not in the word (gray)
  # Convention: 
  #  * Green:  (letter, [1-5]) 
  #  * Orange: (letter, 0 -> unknown, -5 to -1: Not in the negative position.)
  #  * Gray:   (letter, none)
  letter=None
  spot=None

  def __init__(self, letter: str, spot: int):
    assert isinstance(letter, (str, ))
    assert isinstance(spot, (int, )) or spot is None
    self.letter = letter
    self.spot = spot

  def __repr__(self) -> str:
    if self.spot is None:
      return f"{self.letter} is not in the word."
    if self.spot == 0: 
      return f"{self.letter} is in the word somewhere."
    if self.spot < 0: 
      return f"{self.letter} is in the word, but not in position {-self.spot}."
    return f"{self.letter} is in position {self.spot}."

  def __eq__(self, other):
    spots_equal = (self.spot is None and other.spot is None) or (self.spot == other.spot)
    return spots_equal and (self.letter == other.letter)

  def __hash__(self):
    return hash((self.letter,self.spot))

  def is_possible(self, word: str) -> bool:
    """Checks if the given word is possible given our knowledge
    """
    if self.spot is None:
      return self.letter not in word
    if self.spot == 0:
      return self.letter in word
    if self.spot < 0:
       return (self.letter in word) and (word[-self.spot-1] != self.letter)
    
    # Remember to subtract one because python indexes at zero and humans use 1.
    return word[self.spot-1] == self.letter


class KnowledgeSet(set):
  """Manage sets of Knowledge elements. 
  Each Knowledge element describes a single bit of knowledge about letters
  """

  hard_mode = False

  @property
  def not_present(self) -> "KnowledgeSet":
    return self.__class__([x for x in self if x.spot is None])

  @property
  def present(self)  -> "KnowledgeSet":
    return self.__class__([x for x in self if isinstance(x.spot, int)])

  @property 
  def green(self)  -> "KnowledgeSet":
    return self.__class__([x for x in self if x.spot is not None and x.spot > 0])

  @property 
  def orange(self)  -> "KnowledgeSet":
    return self.__class__([x for x in self if x.spot is not None and x.spot <= 0])
  
  @property
  def unknown_letters(self) -> str:
    return set(ascii_lowercase) - set([x.letter for x in self])

  def get_spot(self, spot):
    return [x for x in self if x.spot == spot]

  def clean(self):
    """Sometimes, things get messed up.  Clean them.
    """
    # If a guess has a double letter and one is in the right place,
    # the other is marked gray. Fix this by removing the gray letter.
    removals = set()
    conversions = set()
    for x in self:
      if (x.spot is not None) and (Knowledge(x.letter, None) in self):
        # If we know this letter is somewhere, there's only one. 
        if x.spot < 0: 
          info (f"Removing 'no {x.letter}' because we also learned {x.letter} exists but is NOT in {-x.spot}")
          removals.add(Knowledge(x.letter, None))
        # If we know this letter is somewhere, it's not in any unknown spot.
        if x.spot > 0:
          info (f"Converting 'no {x.letter}' because {x.letter} is in {x.spot}")
          conversions.add(Knowledge(x.letter, None))
    for x in conversions:
      uks = self.unknown_spots()
      self.remove(x)
      [self.add_knowledge(x.letter, -spot) for spot in uks]
    for x in removals: 
      if x in self: 
        self.remove(x)

  def unknown_spots(self):
    spots = set([1,2,3,4,5])
    [spots.remove(x.spot) for x in self if x.spot in spots]
    return spots

  def is_possible(self, word: str) -> bool:
    """Calculate if this knowledge set permits a word.

    """
    return all([x.is_possible(word) for x in self])

  def letter_histogram(self, wordset: List[str]):
    possible_words = [x for x in wordset if self.is_possible(x)]
    return letter_histogram(possible_words)

  def add_knowledge(self, letter: str, spot: int = 0):
    """Adds something we know to the knowledge store

    letter: The letter we know about
    spot: 1-5  -> Known position 
          0    -> Unknown
          none -> Not present
    """
    # Check that we're not saying the same location has two different letters.
    if (spot in {1,2,3,4,5}) and any([(x.spot==spot and not x.letter==letter) for x in self]):
      raise ValueError(f"Attempt to add duplicate knowledge about position {spot}")

    # TODO: Check that we don't have a letter both existing and not existing.  
    
    self.add(Knowledge(letter, spot))

  def __repr__(self) -> str:
    result = ["KnowledgeSet Summary:"]
    np = self.not_present
    if len(np):
      result.append(f"""  These letters are not present: {", ".join(sorted([x.letter for x in np]))}""" )
    orange = self.orange
    if len(orange):
      result.append(f"""  These letters are present somewhere: {", ".join(sorted([f"{x.letter} (not {-x.spot})" for x in self.orange]))}""")
    if len(self.green):
      base = ["?"] * 5
      for x in self.green:
        base[x.spot-1] = x.letter
      result.append(f"""  Current knowledge: {"".join(base)}""")
    return "\n".join(result)
  
