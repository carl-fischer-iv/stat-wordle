# Wordle-related utilities.

from math import log2
from collections import defaultdict
from collections.abc import Iterable
from typing import Dict, List
from datetime import datetime

WORDLE_EPOCH = datetime(2021,6,19)

def get_index(date=None):
  """get_index gets the index for today or a given day.
  
  return int: The Wordle day index.
  """
  date = date or datetime.now()
  return (date - WORDLE_EPOCH).days


def entropy(word: str, hists: List[Dict[str, float]]) -> float:
  """Tests a word against this bit of knowledge
  """
  
  # Calculate this word's entropy score based on the histograms given for each letter.
  entropy = 0
  for letter, hist in zip(word, hists):
    # Add one to spot because python counts from 0 & humans count from 1
    p = hist.get(letter, 0) #self.score_char(letter, spot+1, hist) 
    if p > 0:
      entropy -= p * log2(p)

  # Add this word's internal entropy (discourages double-letters).
  # TODO: Add known letters here??
  word_hist = letter_histogram(word)
  for val in word_hist.values():
    if val > 0:
      entropy -= val * log2(val)
  
  return entropy

def word_filter(word):
  """Returns true or false depending on whether we should keep a word.
  """
  return len(word)==5 and word.isalpha()

def letter_histogram(letters: Iterable) -> Dict:
  """Computes a histogram forom given letters
  """
  # histresult = dict([(a, 0) for a in ascii_lowercase])
  hist = defaultdict(lambda: 0)
  llen = len(letters)

  for l in letters: 
    hist[l] += 1.0/llen
  # Squash the defaultdict now we're done.
  return dict(hist)
