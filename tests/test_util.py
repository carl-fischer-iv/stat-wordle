from math import log2
import unittest
from statwordle.util import entropy, letter_histogram, word_filter
from string import ascii_lowercase

class TestUtils(unittest.TestCase):
    def setUp(self):
        self.alpha_hist = letter_histogram(ascii_lowercase)
        
    def test_letter_histogram(self):
        frequency = 1.0/len(ascii_lowercase)
        self.assertEqual(set(ascii_lowercase), set(self.alpha_hist.keys()), 
            "hist values should match input letter set")
        self.assertTrue(all([x == frequency for x in self.alpha_hist.values()]),
            "Each letter is in the alphabet onec, so its frequency is 1/26")
    
    def test_entropy(self):
        p = 1.0/len(ascii_lowercase)
        # Entropy is computed for each letter vs. its histogram, then across the word.
        abcde_entropy = (-p * log2(p)) * 5 + (-(0.2) * log2(0.2)) * 5

        self.assertEqual(entropy('abcde', [self.alpha_hist]*5), abcde_entropy,
            "Entropy calculation confirmed.")

    def test_word_filter(self):
        """Checks if a word might be a wordle word."""
        self.assertTrue(word_filter('apple'),
            "Apple has 5 letters.")
        self.assertTrue(word_filter('banan'),
            "banan has 5 letters.")
        self.assertTrue(word_filter('Mango'),
            "Mango has a caps! has 5 letters.")
        self.assertFalse(word_filter('Turtle'),
            "Turtle has too many letters.")

if __name__ == '__main__':
    unittest.main()
