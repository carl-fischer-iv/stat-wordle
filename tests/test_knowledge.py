import unittest
from statwordle.knowledge import KnowledgeSet
from statwordle.wordle import Knowledge, WordleSolver
from statwordle.util import entropy, letter_histogram, word_filter
from os import path
# from bz2 import open as bz2open
from random import choice
from string import ascii_lowercase

from statwordle.words import DICTIONARY

FILE_LOC = f"{path.dirname(__file__)}/../src/statwordle/five-letters.txt.bz2"


def aletter() -> str:
    return choice(ascii_lowercase)


def aspot() -> str:
    return choice([1, 2, 3, 4, 5])


class TestKnowledge(unittest.TestCase):
    def setUp(self) -> None:
        self.cases = {
            'no_a': Knowledge('a', None),
            'no_z': Knowledge('z', None),
            'has_a': Knowledge('a', 0),
            'has_z': Knowledge('z', 0),
            'a_not_2': Knowledge('a', -2),
            'z_not_2': Knowledge('z', -2),
            'a_at_2': Knowledge('a', 2),
            'z_at_2': Knowledge('z', 2)
        }

    def test_descriptions(self):
        answers = {k: repr(v) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], 'a is not in the word.')
        self.assertEqual(answers['no_z'], 'z is not in the word.')
        self.assertEqual(answers['has_a'], 'a is in the word somewhere.')
        self.assertEqual(answers['has_z'], 'z is in the word somewhere.')
        self.assertEqual(answers['a_not_2'], 'a is in the word, but not in position 2.')
        self.assertEqual(answers['z_not_2'], 'z is in the word, but not in position 2.')
        self.assertEqual(answers['a_at_2'], 'a is in position 2.')
        self.assertEqual(answers['z_at_2'], 'z is in position 2.')


    def test_apple_is_possible(self):
        word = 'apple'
        answers = {k: v.is_possible(word) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], False)
        self.assertEqual(answers['no_z'], True)
        self.assertEqual(answers['has_a'], True)
        self.assertEqual(answers['has_z'], False)
        self.assertEqual(answers['a_not_2'], True)
        self.assertEqual(answers['z_not_2'], False)
        self.assertEqual(answers['a_at_2'], False)
        self.assertEqual(answers['z_at_2'], False)

    def test_banal_is_possible(self):
        word = 'banal'
        answers = {k: v.is_possible(word) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], False)
        self.assertEqual(answers['no_z'], True)
        self.assertEqual(answers['has_a'], True)
        self.assertEqual(answers['has_z'], False)
        self.assertEqual(answers['a_not_2'], False)
        self.assertEqual(answers['z_not_2'], False)
        self.assertEqual(answers['a_at_2'], True)
        self.assertEqual(answers['z_at_2'], False)

    def test_bangs_is_possible(self):
        word = 'bangs'
        answers = {k: v.is_possible(word) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], False)
        self.assertEqual(answers['no_z'], True)
        self.assertEqual(answers['has_a'], True)
        self.assertEqual(answers['has_z'], False)
        self.assertEqual(answers['a_not_2'], False)
        self.assertEqual(answers['z_not_2'], False)
        self.assertEqual(answers['a_at_2'], True)
        self.assertEqual(answers['z_at_2'], False)

    def test_brags_is_possible(self):

        word = 'brags'
        answers = {k: v.is_possible(word) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], False)
        self.assertEqual(answers['no_z'], True)
        self.assertEqual(answers['has_a'], True)
        self.assertEqual(answers['has_z'], False)
        self.assertEqual(answers['a_not_2'], True, "Brags has an A in position 3.")
        self.assertEqual(answers['z_not_2'], False)
        self.assertEqual(answers['a_at_2'], False, "Brags does not have an A at 2.")
        self.assertEqual(answers['z_at_2'], False)

    def test_zebra_is_possible(self):
        word = 'zebra'
        answers = {k: v.is_possible(word) for k, v in self.cases.items()}
        self.assertEqual(answers['no_a'], False)
        self.assertEqual(answers['no_z'], False)
        self.assertEqual(answers['has_a'], True)
        self.assertEqual(answers['has_z'], True)
        self.assertEqual(answers['a_not_2'], True)
        self.assertEqual(answers['z_not_2'], True)
        self.assertEqual(answers['a_at_2'], False)
        self.assertEqual(answers['z_at_2'], False)

    def test_equal(self):
        self.assertEqual(self.cases['no_a'], Knowledge('a', None))
        self.assertEqual(self.cases['a_not_2'], Knowledge('a', -2))
        self.assertEqual(self.cases['a_at_2'], Knowledge('a', 2))

class TestKnowledgeSet(unittest.TestCase):
    def setUp(self) -> None:
        self.cases = {
            'no_a': Knowledge('a', None),
            'no_z': Knowledge('z', None),
            'has_a': Knowledge('a', 0),
            'has_z': Knowledge('z', 0),
            'a_not_2': Knowledge('a', -2),
            'z_not_2': Knowledge('z', -2),
            'a_at_2': Knowledge('a', 2),
            'z_at_2': Knowledge('z', 2)
        }
        self.knowledgeset = KnowledgeSet(self.cases.values())


if __name__ == '__main__':
    unittest.main()
