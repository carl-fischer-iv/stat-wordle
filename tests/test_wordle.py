import unittest
from statwordle.wordle import Knowledge, WordleSolver
from statwordle.util import entropy, letter_histogram, word_filter
from os import path
# from bz2 import open as bz2open
from random import choice
from string import ascii_lowercase

from statwordle.words import DICTIONARY

FILE_LOC = f"{path.dirname(__file__)}/../src/statwordle/five-letters.txt.bz2"


def aletter() -> str:
    return choice(ascii_lowercase)


def aspot() -> str:
    return choice([1, 2, 3, 4, 5])
class TestWordleSolver(unittest.TestCase):
    def setUp(self) -> None:
        self.five_letters = DICTIONARY

        return super().setUp()

    def get_fresh_solver(self) -> WordleSolver:
        return WordleSolver(self.five_letters)

    def test_suggest_guesses(self):
        """After initializing with the full dictionary, there are many words"""
        ws = self.get_fresh_solver()
        self.assertEqual(len(ws.suggest_guesses(5)), 5)
        self.assertIsInstance(ws.suggest_guesses(1)[0], (str, ) )
        ws.add_guess('apple', 'ggggg')
        self.assertEqual(ws.suggest_guesses(10), ['apple'])


    def test_many_possible_words(self):
        """After initializing with the full dictionary, there are many words"""
        ws = self.get_fresh_solver()
        self.assertEqual(ws.possible_words, set(self.five_letters),
                         "All words should be possible before filtering.")

    def test_located_letter(self):
        """After initializing with the full dictionary, there are many words"""
        ws = self.get_fresh_solver()
        letter = aletter()
        ws.add_knowledge(letter, 1)
        self.assertLess(len(ws.possible_words), len(self.five_letters),
                        "Adding knowledge should reduce the set of words.")
        self.assertTrue(all([x[0] == letter for x in ws.possible_words]),
                        f"All words should start with the chosen letter {letter}")

    def test_detected_letter(self):
        """Test behavior when a single letter is known"""
        ws = self.get_fresh_solver()
        letter = aletter()
        ws.add_knowledge(letter, 0)
        self.assertLess(len(ws.possible_words), len(self.five_letters),
                        "Adding knowledge should reduce the set of words.")
        self.assertTrue(all([letter in x for x in ws.possible_words]),
                        f"All words should contain the chosen letter {letter}")

    def test_rejected_letter(self):
        """Test behavior when a single letter is known to not be in the word"""
        ws = self.get_fresh_solver()
        letter = aletter()
        ws.add_knowledge(letter, None)
        self.assertLess(len(ws.possible_words), len(self.five_letters),
                        "Adding knowledge should reduce the set of words.")
        self.assertTrue(all([letter not in x for x in ws.possible_words]),
                        f"No words should contain the chosen letter {letter}")

    def test_duplicate_spot_error(self):
        """Test that an error is thrown when we declare two different letters in a spot"""
        ws = self.get_fresh_solver()
        ws.add_knowledge('a', 1)
        with self.assertRaises(ValueError,
                               msg="Declaring conflicting values for the same spot SHOULD raise ValueError"):
            ws.add_knowledge('b', 1)

    def test_is_possible(self):
        """Test is_possible with a few words"""
        ws = self.get_fresh_solver()
        ws.add_knowledge('c', 1)
        self.assertTrue(ws.is_possible('color'))
        self.assertFalse(ws.is_possible('apple'))
        self.assertFalse(ws.is_possible('acccc'))

    def test_possible_sets(self):
        """Test the possible sets"""
        letter = aletter()
        spot = aspot()

        ws = self.get_fresh_solver()
        ws.add_knowledge(letter, spot)
        possible_sets = ws.get_possible_sets()
        self.assertEqual(len(possible_sets[spot-1]), 1,
                         msg="There should only be one possible letter in the chosen spot.")

        spots = [1, 2, 3, 4, 5]
        spots.remove(spot)

        for s in spots:
            print(f"Testing {s}, spot is {spot}")
            self.assertGreater(len(possible_sets[s-1]), 1,
                               msg="Unconstrained spots should have several options.")

    def test_possible_histograms(self):
        """Test the possible sets"""
        letter = aletter()
        spot = aspot()

        ws = self.get_fresh_solver()
        ws.add_knowledge(letter, spot)
        possible_hists = ws.get_possible_histograms()
        self.assertEqual(len(possible_hists[spot-1]), 1,
                         msg="There should only be one possible letter in the chosen spot.")

    def test_hard_series(self):
        """Some particularly tricky word guesses cause problems."""
        guess_history = [('pares', '-g---'), ('manky', '-g--g'), ('fatly', '-gg-g'), ('tatty', '-gggg')]

        ws = self.get_fresh_solver()
        for guess, result in guess_history:
            ws.add_guess(guess, result)


if __name__ == '__main__':
    unittest.main()
