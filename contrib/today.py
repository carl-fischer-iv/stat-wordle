from statwordle.html import write_html, STYLE
from statwordle.words import NYT_ANSWERS, OG_ANSWERS
from statwordle.wordle import PlayGame
from statwordle.util import get_index, WORDLE_EPOCH
from datetime import timedelta

style = (STYLE)
N = 0
censor = False

today_ndx = get_index()
ndx = get_index()
l = min(len(OG_ANSWERS), len(NYT_ANSWERS))
if ndx >= l - N:
    ndx = l - N



def describe(wordle_day):
    day = WORDLE_EPOCH + timedelta(days=wordle_day)
    return f"{wordle_day} ({day.strftime('%b %d, %Y')})"

def content(wordle_day, html_class="", censor=False):
    if (wordle_day < ndx) or not censor:
        answer = f"""<p><b>NYT/OG Answers:</b> {NYT_ANSWERS[wordle_day]} / {OG_ANSWERS[wordle_day]}</p>"""
    else:
        answer = f"""<p><b>NYT/OG Answers:</b> 
        <a href="#" data-toggle="tooltip" title="{NYT_ANSWERS[wordle_day]} / {OG_ANSWERS[wordle_day]}">CENSORED (mouseover for answer)</a> </p>"""
    return f"""
    <div class="container {html_class}" data-value="{ndx}">
        <div class="row">
            <div class="col-sm-12">
                <h3 id="ans-{wordle_day}">Day {describe(wordle_day)}</h3>
                {answer}
            </div>
        </div>
        {make_daily_content(wordle_day, censor=censor)}
    </div>
    """


def make_daily_content(ndx, censor=True, html_class=""):
    censor = censor and (ndx >= today_ndx)
    ans = NYT_ANSWERS[ndx]
    # Only pick the best choice. Play in hard mode.
    pg = PlayGame(ans, n_choice=1, hard_mode=True)
    num = pg.play()
    nyt_game_results = pg.wg.show_results(censor=censor)
    nyt_shared_results = pg.wg.shared_results(ndx=ndx)

    if OG_ANSWERS[ndx] == NYT_ANSWERS[ndx]:
        og_game_results = nyt_game_results
        og_shared_results = nyt_shared_results
    else:
        ans = OG_ANSWERS[ndx]
        # Only pick the best choice. Play in hard mode.
        pg = PlayGame(ans, n_choice=1, hard_mode=True)
        num = pg.play()
        og_game_results = pg.wg.show_results(censor=censor)
        og_shared_results = pg.wg.shared_results()

    return f"""
        <div class="row">
            <div class="col col-sm">
                <p>NYT Wordle {ndx} {num}/6 </p>
                {nyt_game_results}
            </div>
            <div class="col col-xs">NYT {nyt_shared_results}</div>

            <div class="col col-sm">
                <p>OG Wordle {ndx} {num}/6</p>
                {og_game_results}
            </div>
            <div class="col col-xs">OG {og_shared_results}</div>
        </div>
    """


if __name__ == "__main__":

    today = (content(ndx))

    content=("""
<div class="container">
    <div class="row">
        <div class="col-sm">
            <h1>Stat-Wordle Performance</h1>

            <p>This document shows today's automated guess uncensored for  
            <a href="https://gitlab.com/carl-fischer-iv/stat-wordle">stat-wordle</a>. 
            <p>Browse historical results <a href="index.html">here</a>.
        </div>
    </div>
</div>""")

    write_html('public/today.html', 
            content="".join([content, style, today]),
            title=f'Uncensored Stat-Wordle Results for {describe(ndx)}')
