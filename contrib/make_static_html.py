from statwordle.html import write_html, STYLE
from statwordle.words import OG_ANSWERS, NYT_ANSWERS, DICTIONARY_ENTROPY
from statwordle.wordle import PlayGame
from statwordle.util import get_index
from today import make_daily_content, describe, content

from ipywidgets.widgets import Stacked, Dropdown, HTML, jslink
from ipywidgets.embed import embed_minimal_html

style = (STYLE)
N = 5

ndx = get_index()
l = min(len(OG_ANSWERS), len(NYT_ANSWERS))
if ndx >= l - N:
    ndx = l - N

rng = range(ndx+N, ndx+N-100, -1)

topmatter = ("""
    <div class="row">
        <div class="col-sm">
            <h1>Stat-Wordle Performance</h1>

            <p>This document allows you to browse the latest guess performance of 
            <a href="https://gitlab.com/carl-fischer-iv/stat-wordle">stat-wordle</a>. 
            You can see performance up to 5 days ahead of today by changing the dropdown 
            selector. Answers for today and future days are redacted to avoid spoilers.
            <p><b>Spoiler Alert</b> <a href="today.html">Today's uncensored result</a>
        </div>
    </div>
""")

def nav(i):
    return f"""
    <a class="nav-link" href="#ans-{i}">{describe(i)}</a>
    """

sidebar = (f"""
<div class="col-md-2 bd-sidebar overflow-auto navbar-expand-md px-0">
    <div class="container-fluid d-flex flex-column flex-grow-1 vh-100 overflow-auto">

        <h5>Navigation</h5>
        <nav class="navbar flex-column nav-pills" id="ans-nav">
            { "".join([nav(i) for i in rng]) }
        </nav>
    </div>
</div>
""")

scroll_content = (f"""
{ "".join([content(x, censor=True, html_class="option") for x in rng]) }
""")

content = f"""
{style}
<div class="container-fluid d-flex flex-column flex-grow-1 vh-100">
    <div class="row flex-xl-nowrap">
    {sidebar}
        <div class="col-sm">
            <div data-spy="scroll" data-target="#ans-nav" data-offset="0" class="container-fluid d-flex flex-column flex-grow-1 vh-100 overflow-auto">
                {topmatter}
                {scroll_content}
            </div>
        </div>
    </div>
</div>
"""

write_html('public/index.html', 
            content=content,
            title=f'Stat-Wordle Results {describe(ndx)}')
