# Stat Wordle

This package does analysis of statistics in Wordle. 

. . . Because it's interesting.

## Installation: 

``` bash
pip install stat-wordle --extra-index-url https://gitlab.com/api/v4/projects/32874765/packages/pypi/simple
```

## Usage

To play a game where you know the answer:

``` python
from statwordle.wordle import WordleGame
wg = WordleGame('guess')
clue = wg.handle_guess('cares')
# clue equals '---og'
```

To have the algorithm make suggestions:

``` python
from statwordle.wordle import WordleSolver
from statwordle.words import DICTIONARY

ws = WordleSolver(DICTIONARY)
ws.add_guess('cares', '---og')

```
